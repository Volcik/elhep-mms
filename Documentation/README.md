# Specification for the JSON configuration file for MMS (Mobile Measurement System) v 0.1 (ELHEP)

![ELHEP Logo](../iOS/Icon.png)

---

## Connection algorithm 

1. Mobile Device such as an iOS, Android or Windows Phone device is connected to WiFi network

2. Upon opening MMS appliction, Mobile Device sends broadcast UDP packets in the SSDP format with SEARCH method (look into ELHEP/Documentation/3party/UPnP-arch-DeviceArchitecture-v1.1.pdf, page 31)

3. When there are some WiFI - connected MMS capable embedded devices that received the SSDP search packet, they should send SSDP response as specified in the  UPnP-arch-DeviceArchitecture, page 33

4. Mobile device is listening at port 1900 and when it receives SSDP respone, it now knows the IP addresses of the all Measurement Devices in the local network, it should display list of the available device to the user 

5. Mobile device should download JSON configuration file from all of the Measurement Devices in the local network (URL for JSON file should be for example XXX.XXX.XXX.XXX/conf.json) via HTTP servers running on the Measurment Devices

6. Mobile device should parse JSON file and generate GUI based on it.

7. User of the Mobile device can now interact with the GUI and change all parameters in the 'probeIn' JSON node

8. If user want to start receiving data from Measurement device, he should press 'start' button in the GUI

9. Now, mobile device sends small part of the JSON conf file to the proper Measurement Device with changed parameters in the 'probeIn' node

10. If the Measurement Device agrees that JSON it received is correct, status JSON is sent and the measurements start. If there was an error, small JSON with error description and the disconnects. Measurement data should be sent to Mobile Device and displayed to the user on the generated, dynamic, animated GUI, with the possibility to send actions specified in the 'probeActions' node


---
## Sample images

Sample images for iOS client for MMS interface

![image1](img/1.main_menu-kopia.png)
![image2](img/2.device_spec-kopia.png)
![image3](img/3.measurement_screen-kopia.png)

---
## Sample files 

1. sample configuration    file that can be downloaded from device (by WebSocket): ELHEP/docs/json_device.json

		{   "deviceID" : 12,
			"verID" : 0.1,
			"deviceName" : "Multimeter Siemens C234",
				"uniqueProbes" : [
				{
					"probeID" : 10,
					"probeName" : "ADC2131",
					"probeSpec" : {
						"probeOut" : {
							"probeType" : "A",
							"dataType" : "d1",
							"dataUnit" : "mA",
							"defaultDisplayType" : "label"},
						"probeIn" : {
							"sampleFreq" : 10,
							"autoTriggerRetry" : true,
							"numberOfSamples" : 100},
						"probeActions" : [
						{
							"actionName" : "Turn On After",
							"actionParameter" : "d1",
							"actionDesc" : "Time when the light should go on"
						}]
					}
				},
				{
					"probeID" : 14,
					"probeName" : "TCDS213",
					"probeSpec" : {
						"probeOut" : {
							"probeType" : "V",
							"dataType" : "d2",
							"dataUnit" : "MV",
							"defaultDisplayType" : "plot"},
						"probeIn" : {
							"sampleFreq" : 1000,
							"autoTriggerRetry" : false,
							"numberOfSamples" : 10},
						"probeActions" : [
						{
							"actionName" : "Set temperature",
							"actionParameter" : "d1",
							"actionDesc" : "Set temprature"
						}]
					}
				}]
		}


2. sample configuration    file that can be sent to device: ELHEP/docs/json_answer.json
		
		{   "deviceID" : 12,
				"uniqueProbes" : [
				{
					"probeID" : 10,
					"probeSpec" : {
						"probeIn" : {
							"sampleFreq" : 10,
							"autoTriggerRetry" : true,
							"numberOfSamples" : 100}
					}
				},
				{
					"probeID" : 14,
					"probeSpec" : {
						"probeIn" : {
							"sampleFreq" : 1000,
							"autoTriggerRetry" : false,
							"numberOfSamples" : 10}
					}
				}]
		}



3. sample measurement data file that can be sent to device: ELHEP/docs/json_sample_measurement.json

		{   "deviceID" : 12,
				"measurements" : [
				{
					"probeID" : 10,
					"timestamp" : 123124134134134,
					"probeValue" : [
					123, 123,1231,123,123,123,123,123,123,12,312,3123
					]
				},
				{
					"probeID" : 14,
					"timestamp" : 123124134134134,
					"probeValue" : [
					123, 123,1231,123,123,123,123,123,123,12,312,3123
					]
				}]
		}


4. sample status message, sent to aknowledge the paring of two devices

		{
			"status" : 0,
			"error" : "You are too ugly to work with me"
		}

---
## JSON file description

---

node name            - (short name) , type, possible values, notes

---

1. deviceID             - (dID), unsigned, 0 to 65,535, must be unique in the network
2. verID                - (vID), string,x.y.z foramt, version of the MMS API
3. deviceName           - (dN), string, human readable name of the device
4. uniqueProbes         - (uP), array of the Probe objects
5. probeID              - (pID), unsigned, 0 to 65,535, must be unique in current device
6. probeName            - (pN), string, human readable name of the probe
7. probeSpec            - (ps), array of the 3 objects, ProbeIn, ProbeOut, ProbeActions
8. probeType            - (pA), enumerated, short string type, at the moment possible values (default is V):

	1. A - current measurement
	2. V - voltage measurement
	3. T - temperature measurement
	4. P - pressure measurement



9. dataType             - (dT), enumareted, short string type, possible values:

	1. d1 - data returned are 1 dimensional
	2. d2 - data returned are 2 dimensional
	3. d3 - data returned are 3 dimensional

10. dataUnit             - (dU), enumareted, short string type, Si units of measured thing

	1. ... uA,mA,A,kA,MA,GA ...
	2. ... uV,mV,V,kV,MV,GV ...
	3. ... mK,K,kK ...

11. defaultDisplayType   - (dDT), default control in the GUI that should be used to display             
                       given measurement data, default is "label" possible values:
	1. label - simple text with the measurement result and unit
	2. chart - chart with measurement value on the y-axis and time on the x-axis
	3. progressBar - progress bar, can be used only when data are scaled to be between 0 and 1

12. sampleFreq           - (sF), float, sampling frequency of the given probe, in Hertz
13. autoTriggerRetry     - (aTR), Boolean, if 'true' measuring device is sending measurment data to 
                        mobile device without trigger, if 'false', mobile device have to ask every time for data
14. numberOfSamples      - (nOS), unsigned long, number of samples that should be accumulated before sending them to the mobile device


------------------------------------------------------------------------------------------------
## Change log


- 29.08.2012 - t.kuzma - first version with nodes description and simple connection algorithm description finished

- 12.09.2012 - t.kuzma - added missing part of the connection algorithm (succes and failure messages)

- 03.10.2012 - t.kuzma - transition to markdown

- 04.10.2012 - t.kuzma - added images and code samples

