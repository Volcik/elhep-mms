//
//  ProbeIn+Utility.h
//  ELHEP
//
//  Created by Tomasz Kuźma on 11/16/12.
//  Copyright (c) 2012 Tomasz Ku≈∫ma. All rights reserved.
//

#import "ProbeIn.h"

@interface ProbeIn (Utility)

- (NSDictionary *)configuration;

@end

@interface NSMutableDictionary (ProbeIn)

- (void)TKsetObject:(id)anObject forKeyIfNotNil:(id <NSCopying>)aKey;

@end
