//
//  WUTDeviceDetailsViewControllerViewController.m
//  ELHEP
//
//  Created by Tomasz Kuźma on 5/30/12.
//  Copyright (c) 2012 Tomasz Kuzma (mapedd@mapedd.com) Warsaw University of Technology. All rights reserved.
//

#import "WUTDeviceDetailsViewController.h"
#import "WUTMeasurementsViewController.h"
#import "TKMainViewController.h"
#import "Device+Utility.h"
#import "Probe.h"
#import "ProbeActions.h"
#import "ProbeIn.h"
#import "ProbeOut+Utility.h"
#import "ProbeAction.h"

#import "WUTDiscovery.h"
#import "WUTSwitchTableViewCell.h"
#import "WUTTextFieldCell.h"

#define TEXT_FIELD_TAG 111
#define SWITCH_TAG 112
#define BUTTON_TAG 113


@interface WUTDeviceDetailsViewController (){
    
    NSTimeInterval animationDuration;
    UIViewAnimationCurve animationCurve;
    CGRect keyboardEndFrame;
}
@property (strong,  nonatomic) Device *device;
@property (strong)UITextField *currentTextField;

@property (strong, nonatomic) NSArray *uniqueProbesArray;

@property (strong, nonatomic) NSSortDescriptor *probeNameDescriptor;
@property (strong, nonatomic) NSSortDescriptor *probeActionDescriptor;
@end

@implementation WUTDeviceDetailsViewController

- (id)initWithDevice:(Device *)device{
    self = [super initWithNibName:nil bundle:nil];
    if (!self) return nil;
    
    self.device = device;
    
    self.probeNameDescriptor = [[NSSortDescriptor alloc] initWithKey:@"probeName" ascending:YES];
    self.probeActionDescriptor = [[NSSortDescriptor alloc] initWithKey:@"actionName" ascending:YES];
    
    return self;
}

- (void)unloadView{
    _device = nil;
    _tableView = nil;
}

- (void)viewDidLoad{
    [super viewDidLoad];
    
    self.title = [_device deviceName];
    
    NSError * __autoreleasing error = nil;
    self.uniqueProbesArray = [self.device.uniqueProbes sortedArrayUsingDescriptors:[NSArray arrayWithObject:self.probeNameDescriptor]];
    
    NSAssert(self.uniqueProbesArray!=nil, @"unique probes array is nil");
    
    if (!_uniqueProbesArray) {
        NSLog(@"error fetching: %@", [error localizedDescription]);
        return;
    }


    self.view.autoresizesSubviews = YES;
    self.tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.sectionHeaderHeight = 44.0f;
    _tableView.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:_tableView];
    
    UIBarButtonItem *startButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Start", nil)
                                                                    style:UIBarButtonItemStylePlain 
                                                                   target:self
                                                                   action:@selector(startCapturingData)];
    
    self.navigationItem.rightBarButtonItem = startButton;
    
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShow:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidHide:)
                                                 name:UIKeyboardDidHideNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

#pragma mark - Actions

- (void)startCapturingData{
    
    if(![self.discovery startMeasurementsWithDevice:self.device]){
        [UIAlertView showAlertWithTitle:NSLocalizedString(@"Error", @"Error") message:@"Can't start measurements"];
        return;

    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(deviceConnected:)
                                                 name:kWUTDeviceConnectedNotification
                                               object:nil];
    

}

- (void)deviceConnected:(NSNotification *)notification{
    
    Device *device = [notification object];
    
    if (device.deviceID.integerValue == self.device.deviceID.integerValue) {
         
        WUTMeasurementsViewController *vc = [[WUTMeasurementsViewController alloc] init];
        vc.delegate = (id<WUTMeasurementsViewControllerDelegate>)self;
        vc.socket = notification.userInfo[@"socket"];
        
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:vc];
        [self.navigationController presentModalViewController:navController animated:YES];
    }
}


#pragma mark - Private

- (NSIndexPath *)indexPathForCurrentTextField{
    if (!_currentTextField) {
        return nil;
    }
    
    CGPoint point = [_tableView convertPoint:_currentTextField.center fromView:_currentTextField.superview];
    
    NSIndexPath *indexPath = [_tableView indexPathForRowAtPoint:point];
    
    return indexPath;
}

- (NSIndexPath *)indexPathForCellSubview:(UIView *)view{
    if (!view) {
        return nil;
    }
    
    CGPoint point  = [_tableView convertPoint:view.center fromView:view.superview];
    
    return [_tableView indexPathForRowAtPoint:point];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return [[self.device uniqueProbes] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    Probe *probe = [_uniqueProbesArray objectAtIndex:section];
    return probe.probeActions.actions.count + 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier             = @"Cell";
    static NSString *TextFieldCellIdentifier    = @"TextFieldCell";
    static NSString *SwitchCellIdentifier       = @"SwitchCell";
    static NSString *CellIdentifierRow0         = @"CellRow0";
    
    Probe *probe = [_uniqueProbesArray objectAtIndex:indexPath.section];
    ProbeIn *probeIn = probe.probeIn;
    ProbeOut *probeOut = probe.probeOut;
    ProbeActions *probeActions = probe.probeActions;

    
    if (indexPath.row == 0) {
        
        UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifierRow0];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifierRow0];
        }
        
        
        NSString *probeType = nil;
        NSInteger dataType = probeOut.dataType.integerValue;
        NSInteger type = probeOut.probeType.integerValue;
        
        
        switch (type) {
            case TYPE_AMPERS:
                probeType = NSLocalizedString(@"Amperemeter", @"Amperemeter");
                break;
            case TYPE_VOLTS:
                probeType = NSLocalizedString(@"Voltmeter", @"Voltmeter");
                break;
            case TYPE_CELSIUS:
                probeType = NSLocalizedString(@"Thermometer", @"Thermometer");
                break;
            default:
                probeType = NSLocalizedString(@"Unknown type", @"Unknown type");
                break;
        }
        
        
        
        cell.textLabel.text = [NSString stringWithFormat:@"%@ ", probeType];
        cell.detailTextLabel.text = [NSString stringWithFormat:@"(%@, %d %@)",probeOut.dataUnit, dataType ,NSLocalizedString(@"dimension", @"dimension")];
        
        return cell;
    }
    else if (indexPath.row == 1) {
        WUTTextFieldCell *cell = (WUTTextFieldCell *)[tableView dequeueReusableCellWithIdentifier:TextFieldCellIdentifier];
        if (cell == nil) {
            cell = [[WUTTextFieldCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:TextFieldCellIdentifier];
        }
            
        cell.textLabel.text = NSLocalizedString(@"Sampling freq.:", @"Sampling freq:");
        cell.textField.text = [NSString stringWithFormat:@"%.2f", [[probeIn sampleFreq] floatValue]];
        cell.unit = WUTUnitHertz;
        cell.textField.delegate = self;
        
        return cell;
    }
    else if (indexPath.row == 2) {
        
        WUTSwitchTableViewCell *cell = (WUTSwitchTableViewCell *)[tableView dequeueReusableCellWithIdentifier:SwitchCellIdentifier];
        if (cell == nil) {
            cell = [[WUTSwitchTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:SwitchCellIdentifier];
        }
        cell.slider.on = [[probeIn autoTriggerRetry]  boolValue];
        cell.textLabel.text = NSLocalizedString(@"Auto trigger", @"Auto trigger");
        [cell addTargetToSwitch:self action:@selector(switchChanged:)];

        return cell;

    }
    else if (indexPath.row == 3) {
        WUTTextFieldCell *cell = (WUTTextFieldCell *)[tableView dequeueReusableCellWithIdentifier:TextFieldCellIdentifier];
        if (cell == nil) {
            cell = [[WUTTextFieldCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:TextFieldCellIdentifier];
        }
        
        cell.textLabel.text = NSLocalizedString(@"Num. of samples.:", @"Num. of samples.:");
        cell.textField.text = [NSString stringWithFormat:@"%d", [[probeIn numberOfSamples] integerValue]];
        cell.textField.delegate = self;
        
        return cell;
    }
    else if (indexPath.row - 4 < probeActions.actions.count) {
        
        NSArray *actions = [probeActions.actions sortedArrayUsingDescriptors:[NSArray arrayWithObject:self.probeActionDescriptor]];
        
        UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
            cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
        }
        
        ProbeAction *action = [actions objectAtIndex:indexPath.row - 4];
        
        cell.textLabel.text = action.actionName;
        
        return cell;
    }
    else {
        return nil;
    }
        
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    NSError * __autoreleasing error = nil;
    NSArray *array = [self.device.uniqueProbes sortedArrayUsingDescriptors:[NSArray arrayWithObject:self.probeNameDescriptor]];
    
    if (!array) {
        NSLog(@"error fetching: %@", [error localizedDescription]);
    }
    
    Probe *probe = [array objectAtIndex:section];
    
    NSString *title = [NSString stringWithFormat:@"%@ %@, ID: %d",NSLocalizedString(@"Title:", @"Title:")
                                                                 ,probe.probeName
                                                                 ,probe.probeID.integerValue];
    
    return title;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50.0f;
}

#pragma mark - WUTMeasurementsViewControllerDelegate

- (void)measurementsViewControllerDidFinish:(WUTMeasurementsViewController *)viewController{
    [self.navigationController dismissModalViewControllerAnimated:YES];
}

#pragma mark - UISwitch

- (void)switchChanged:(UISwitch *)aSwitch{
    NSIndexPath *index = [self indexPathForCellSubview:aSwitch];
    Probe *probe = [_uniqueProbesArray objectAtIndex:index.section];
    ProbeIn *probeIn = probe.probeIn;
    
    probeIn.autoTriggerRetry = [NSNumber numberWithBool:aSwitch.on];
}

#pragma mark - TextField

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    self.currentTextField = textField;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    

    
    NSIndexPath *index = [self indexPathForCurrentTextField];
    Probe *probe = [_uniqueProbesArray objectAtIndex:index.section];
    ProbeIn *probeIn = probe.probeIn;
    probeIn.wasEdited = @YES;
    NSError *error = nil;
    
    if (!textField.text.length || [textField.text floatValue] == 0.0) {
        
        if (index.row == 1) {
            textField.text = [NSString stringWithFormat:@"%.2f", [[probeIn sampleFreq] floatValue]];
        }
        else if(index.row == 3){
            textField.text = [NSString stringWithFormat:@"%d", [[probeIn numberOfSamples] integerValue]];
        }

    }
    else{
        textField.text = [textField.text stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        if (index.row == 1) {
            probeIn.sampleFreq = [NSNumber numberWithFloat:textField.text.floatValue];
        }
        else if(index.row == 3){
            probeIn.numberOfSamples = [NSNumber numberWithInteger:textField.text.integerValue];
        }
        
        if(![probeIn.managedObjectContext save:&error]){
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"Error")
                                                                message:NSLocalizedString(@"Can't save to database, try again", @"Can't save to database, try again")
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
            [alertView show];
        }
        
        
    }
    
    
    
    [textField resignFirstResponder];
    self.currentTextField = nil;
    
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    BOOL shouldChange = NO;
    
    if ([self indexPathForCurrentTextField].row == 1) {
        shouldChange = [self isNumeric:string] || [string isEqualToString:@" "] || [string isEqualToString:@"."];
    }
    else if ([self indexPathForCurrentTextField].row == 3){
        shouldChange = [self isNumeric:string] || [string isEqualToString:@" "];
    }
    
    return shouldChange;
}

- (BOOL)isNumeric:(NSString*)inputString{
    BOOL isValid = NO;
    NSCharacterSet *alphaNumbersSet = [NSCharacterSet decimalDigitCharacterSet];
    NSCharacterSet *stringSet = [NSCharacterSet characterSetWithCharactersInString:inputString];
    isValid = [alphaNumbersSet isSupersetOfSet:stringSet];
    return isValid;
}

#pragma mark - NSNotificationCenter

- (void)keyboardDidShow:(NSNotification *)notification{
    
}

- (void)keyboardDidHide:(NSNotification *)notification{
    
}

-(void)keyboardWillShow:(NSNotification*)notif{
    NSDictionary* userInfo = [notif userInfo];
    
    [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&animationCurve];
    [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardEndFrame];
    
    [self animateKeyboardDidShowWithDuration:animationDuration animationCurve:animationCurve];
}

-(void)keyboardWillHide:(NSNotification*)notif{
    NSDictionary* userInfo = [notif userInfo];
    
    [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&animationCurve];
    [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardEndFrame];
    
    [self animateKeyboardDidHideWithDuration:animationDuration animationCurve:animationCurve];
    
}

- (void)animateKeyboardDidShowWithDuration:(NSTimeInterval)anAnimationDuration
                            animationCurve:(UIViewAnimationCurve)anAnimationCurve{

    [UIView beginAnimations:@"goUp" context:nil];
    [UIView setAnimationCurve:anAnimationCurve];
    [UIView setAnimationDuration:anAnimationDuration];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(animationDidStop:finished:context:)];
    _tableView.frame = CGRectMake(_tableView.frame.origin.x, 0.0f, _tableView.frame.size.width, _tableView.frame.size.height  - keyboardEndFrame.size.height);
    [UIView commitAnimations];
    
    

}

- (void)animateKeyboardDidHideWithDuration:(NSTimeInterval)anAnimationDuration
                            animationCurve:(UIViewAnimationCurve)anAnimationCurve{


        [UIView beginAnimations:@"goDown" context:nil];
        [UIView setAnimationCurve:anAnimationCurve];
        [UIView setAnimationDuration:anAnimationDuration];
    
        _tableView.frame = CGRectMake(_tableView.frame.origin.x, 0.0f, _tableView.frame.size.width, self.view.frame.size.height);
    
    
        [UIView commitAnimations];
}

- (void)animationDidStop:(NSString *)animationID
                finished:(NSNumber *)finished
                 context:(void *)context{
    
    [_tableView scrollRectToVisible:[_tableView convertRect:_currentTextField.frame fromView:_currentTextField] animated:YES];
}

@end
