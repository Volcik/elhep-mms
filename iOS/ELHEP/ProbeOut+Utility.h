//
//  ProbeOut+Utility.h
//  ELHEP
//
//  Created by Tomasz Kuźma on 11/16/12.
//  Copyright (c) 2012 Tomasz Ku≈∫ma. All rights reserved.
//

#import "ProbeOut.h"

#define TYPE_VOLTS 1
#define TYPE_AMPERS 2
#define TYPE_CELSIUS 3

#define PROBE_TYPE_VOLTS [NSNumber numberWithInt:TYPE_VOLTS]
#define PROBE_TYPE_AMPERS [NSNumber numberWithInt:TYPE_AMPERS]
#define PROBE_TYPE_CELSIUS [NSNumber numberWithInt:TYPE_CELSIUS]

@interface ProbeOut (Utility)

@end
