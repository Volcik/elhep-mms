//
//  WUTDeviceSpecificationParser.m
//  ELHEP
//
//  Created by Tomasz Kuźma on 5/30/12.
//  Copyright (c) 2012 Tomasz Kuzma (mapedd@mapedd.com) Warsaw University of Technology. All rights reserved.
//

#import "WUTDeviceSpecificationParser.h"
#import "TKAppDelegate.h"
#import "NSString+NSData.h"
#import "Device+Utility.h"
#import "Probe.h"
#import "ProbeIn.h"
#import "ProbeOut+Utility.h"
#import "ProbeActions.h"
#import "ProbeAction.h"

@interface WUTDeviceSpecificationParser ()

@property (strong, nonatomic) NSData *jsonData;

@property (strong, nonatomic) NSString *jsonFilePath;

@end

@implementation WUTDeviceSpecificationParser

- (id)initWithPath:(NSString *)jsonFilePath{
    self = [super init];
    if (!self) return nil;
    
    self.jsonFilePath = jsonFilePath;
    
    self.jsonData = [NSData dataWithContentsOfFile:self.jsonFilePath];
    
    if (!self.jsonData ) {
        NSLog(@"Can't create NSData object from file at path: %@", self.jsonFilePath);
        return nil;
    }
    
    
    return self;
}

- (id)initWithData:(NSData *)jsonData{
    
    self = [super init];
    
    if (!self) return nil;
    
    self.jsonFilePath = nil;
    
    self.jsonData = jsonData;
    
    if (!self.jsonData ) {
        NSLog(@"Can't create NSData object from file at path: %@", self.jsonFilePath);
        return nil;
    }
    
    
    return self;
}

#pragma mark - Public

- (BOOL)parseJSONForDeviceWithUUID:(NSString *)UUID{
    NSError * __autoreleasing error = nil;
    
    NSDictionary *spec = [NSJSONSerialization JSONObjectWithData:self.jsonData options:0 error:&error];
    
    if (!spec) {
        NSLog(@"Can't create JSON file, error = %@", [error localizedDescription]);
        return NO;
    }
    
    Device *device = [NSEntityDescription insertNewObjectForEntityForName:@"Device"
                                                   inManagedObjectContext:self.managedObjectContext];
    
    device.address = UUID;
    device.isConnected = @(NO);
    device.interface = @(WUTInterfaceBluetooth);
    
    return [self fillDevice:device withParameters:spec];
}

- (BOOL)parseJSONForDeviceWithIPAddress:(NSString *)ipAddress{
    
    NSError * __autoreleasing error = nil;
    
    NSDictionary *spec = [NSJSONSerialization JSONObjectWithData:self.jsonData options:0 error:&error];
    
    if (!spec) {
        NSLog(@"Can't create JSON file, error = %@", [error localizedDescription]);
        return NO;
    }
    
    Device *device = [NSEntityDescription insertNewObjectForEntityForName:@"Device"
                                                   inManagedObjectContext:self.managedObjectContext];
    
    device.address = ipAddress;
    device.interface = @(WUTInterfaceWifi);
    
    return [self fillDevice:device withParameters:spec];
}

#pragma mark - Private

- (NSManagedObjectContext *)managedObjectContext{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    TKAppDelegate *delegate = (TKAppDelegate *)[UIApplication sharedApplication].delegate;
    
    
    NSPersistentStoreCoordinator *coordinator = [delegate persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
        [_managedObjectContext setMergePolicy:NSMergeByPropertyStoreTrumpMergePolicy];
        [_managedObjectContext setUndoManager:nil];
    }
    return _managedObjectContext;
}

- (BOOL)fillDevice:(Device *)device withParameters:(NSDictionary *)spec{
    
    
    device.deviceID = [spec objectForKey:@"deviceID"];
    device.deviceName = [spec objectForKey:@"deviceName"];
    device.verID = [spec objectForKey:@"verID"];
    device.dateAdded = [NSDate date];
    
    NSArray *arrayProbes = [spec objectForKey:@"uniqueProbes"];
    
    for (NSDictionary *probeDict in arrayProbes) {
        Probe *probe = [NSEntityDescription insertNewObjectForEntityForName:@"Probe"
                                                     inManagedObjectContext:self.managedObjectContext];
        probe.probeName = [probeDict objectForKey:@"probeName"];
        probe.probeID = [probeDict objectForKey:@"probeID"];
        
        NSDictionary *probeSpec = [probeDict objectForKey:@"probeSpec"];
        {
            /* ProbeIn */
            NSDictionary *probeInDict = [probeSpec objectForKey:@"probeIn"];
            ProbeIn *probeIn = [NSEntityDescription insertNewObjectForEntityForName:@"ProbeIn"
                                                             inManagedObjectContext:self.managedObjectContext];
            probeIn.triggerLow = [probeInDict objectForKey:@"triggerLow"];
            probeIn.triggerHigh = [probeInDict objectForKey:@"triggerHigh"];
            probeIn.sampleFreq = [probeInDict objectForKey:@"sampleFreq"];
            probeIn.numberOfSamples = [probeInDict objectForKey:@"numberOfSamples"];
            probeIn.autoTriggerRetry = [probeInDict objectForKey:@"autoTriggerRetry"];
            
            probe.probeIn = probeIn;
            /* ProbeIn */
        }
        
        {
            /* ProbeOut */
            NSDictionary *probeOutDict = [probeSpec objectForKey:@"probeOut"];
            ProbeOut *probeOut = [NSEntityDescription insertNewObjectForEntityForName:@"ProbeOut"
                                                               inManagedObjectContext:self.managedObjectContext];
            
            NSString *probeType = [probeOutDict objectForKey:@"probeType"];
            if ([probeType isEqualToString:@"V"]) {
                probeOut.probeType = PROBE_TYPE_VOLTS;
            }
            else if ([probeType isEqualToString:@"A"]) {
                probeOut.probeType = PROBE_TYPE_AMPERS;
            }
            else if ([probeType isEqualToString:@"C"]) {
                probeOut.probeType = PROBE_TYPE_CELSIUS;
            }
            
            NSString *dataType = [probeOutDict objectForKey:@"dataType"];
            
            if([dataType isEqualToString:@"d1"]){
                probeOut.dataType = [NSNumber numberWithInt:1];
            }
            else if([dataType isEqualToString:@"d2"]){
                probeOut.dataType = [NSNumber numberWithInt:2];
            }
            else if([dataType isEqualToString:@"d3"]){
                probeOut.dataType = [NSNumber numberWithInt:3];
            }
            
            NSString *defaultDisplayType = [probeOutDict objectForKey:@"defaultDisplayType"];
            if ([defaultDisplayType isEqualToString:@"plot"]) {
                probeOut.defaultDisplayType = [NSNumber numberWithInt:1];
            }
            else if ([defaultDisplayType isEqualToString:@"label"]) {
                probeOut.defaultDisplayType = [NSNumber numberWithInt:2];
            }
            else if ([defaultDisplayType isEqualToString:@"progress"]) {
                probeOut.defaultDisplayType = [NSNumber numberWithInt:3];
            }
            
            probeOut.dataUnit = [probeOutDict objectForKey:@"dataUnit"];
            
            probe.probeOut = probeOut;
            /* ProbeOut */
        }
        
        {
            /* ProbeActions */
            NSArray *probeActionsArray = [probeSpec objectForKey:@"probeActions"];
            
            if ([probeActionsArray count]) {
                ProbeActions *probeActions = [NSEntityDescription insertNewObjectForEntityForName:@"ProbeActions" inManagedObjectContext:self.managedObjectContext];
                
                for (NSDictionary *probeActionDict in probeActionsArray) {
                    ProbeAction *probeAction = [NSEntityDescription insertNewObjectForEntityForName:@"ProbeAction" inManagedObjectContext:self.managedObjectContext];
                    probeAction.actionName = [probeActionDict objectForKey:@"actionName"];
                    probeAction.actionName = [probeActionDict objectForKey:@"actionDesc"];
                    probeAction.actionParameterType = [self dataTypeStringToNumber:[probeActionDict objectForKey:@"actionParameter"]];
                    [probeActions addActionsObject:probeAction];
                    
                }
                probe.probeActions = probeActions;
            }
            /* ProbeActions */
        }
        
        [device addUniqueProbesObject:probe];
        
        
    }
    
    NSError * __autoreleasing error = nil;
    
    
    if(![self.managedObjectContext save:&error]){
        return NO;
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:NSManagedObjectContextDidSaveNotification object:self.managedObjectContext];
    
    return YES;
}

#pragma mark - Utility

- (NSNumber *)dataTypeStringToNumber:(NSString *)dataType{
    if([dataType isEqualToString:@"d1"]){
        return @(1);
    }
    else if([dataType isEqualToString:@"d2"]){
        return @(2);
    }
    else if([dataType isEqualToString:@"d3"]){
        return @(3);
    }
    else {
        return @(0);
    }
}

@end
