//
//  ProbeOut.m
//  ELHEP
//
//  Created by Tomasz Kuźma on 11/16/12.
//  Copyright (c) 2012 Tomasz Ku≈∫ma. All rights reserved.
//

#import "ProbeOut.h"
#import "Probe.h"


@implementation ProbeOut

@dynamic dataType;
@dynamic dataUnit;
@dynamic defaultDisplayType;
@dynamic probeType;
@dynamic probe;

@end
