//
//  WUTPlotController.m
//  ELHEP
//
//  Created by Tomasz Kuźma on 5/30/12.
//  Copyright (c) 2012 Tomasz Kuzma (mapedd@mapedd.com) Warsaw University of Technology. All rights reserved.
//

#import "WUTPlotController.h"
#import "WUTPlotView.h"
#import "WUTResult.h"
#import "Device+Utility.h"
#import "SRWebSocket.h"
#import "Measurement.h"
#import "../../Common/mms_configuration.h"
#import "DDLog.h"
#import "DDTTYLogger.h"
// Log levels: off, error, warn, info, verbose
static const int ddLogLevel = LOG_LEVEL_VERBOSE;

#define RANDOM_SEED() srandom((unsigned)(mach_absolute_time() & 0xFFFFFFFF))
#define RANDOM_NUM() (random())


@interface WUTPlotController () <WUTPlotViewDataSource>{
    double time;
    double period;
    float samplingFreq;
    
    NSMutableArray *resultsArray_;
}

@property (strong, nonatomic) NSTimer *timer;

@property (strong, nonatomic) WUTPlotView *plotView;

@property (nonatomic, readwrite) BOOL isSendingResults;

@property (nonatomic, strong) SRWebSocket *websocket;

@end

@implementation WUTPlotController

@synthesize plotView =plotView_;

@synthesize timer =timer_;

- (id)initWithPlotView:(WUTPlotView *)plotView{
    self = [self init];
    if(!self) return nil;
    
    self.plotView = plotView;
    self.isSendingResults = NO;
    
    plotView.dataSource = self;
    
    return self;
} 

- (id)init{
    self = [super init];
    if(!self) return nil;
    
    time = 0;
    period = 10;
    samplingFreq = 0.01;
    resultsArray_ = [[NSMutableArray alloc]initWithCapacity:0];
    
    return self;
}

- (void)startSendingResults{
    self.timer = [NSTimer scheduledTimerWithTimeInterval:samplingFreq
                                     target:self 
                                   selector:@selector(sendResult)
                                   userInfo:nil 
                                    repeats:YES];
    self.isSendingResults = YES;
}

- (void)stopSendingResults{
    
    if (self.websocket) {
        [self.websocket close];
    }
    
    if (self.timer) {
        [self.timer invalidate];
        self.timer = nil;
    }
    
    self.isSendingResults = NO;
}

- (CGFloat)sinValueForTime:(double)aTime{
    return (sin(aTime/period) + 1) * 0.5;
}

- (CGFloat)sinexValueForTime:(double)aTime{
    CGFloat exp = expf(-aTime);
    
    if (exp < 0.1) {
        exp = expf(aTime);
    }
    
    if(exp > 1 ){
        exp = expf(-aTime);
    }
    
    return exp * (sin(aTime/period) + 1) * 0.5;
}

- (void)sendResult{
    WUTResult *result = [[WUTResult alloc] initWithValue:[self sinValueForTime:time] andTimeStamp:[NSDate date]];
    [resultsArray_ addObject:result];
    time ++;
    
    [self.plotView setNeedsDisplay];
}

- (void)sendMeasurement:(Measurement *)measurement{
    
    CGFloat sampleValue = ((int)floorf(measurement.sample) % 300) / 300;
    
    WUTResult *result = [[WUTResult alloc] initWithValue:sampleValue andTimeStamp:[NSDate dateWithTimeIntervalSince1970:measurement.timestamp]];
    [resultsArray_ addObject:result];
    
    time ++;
    
    [self.plotView setNeedsDisplay];
}

- (void)setIsSendingResults:(BOOL)isSendingResults{
    if (_isSendingResults != isSendingResults) {
        [self willChangeValueForKey:@"isSendingResults"];
        _isSendingResults = isSendingResults;
        [self didChangeValueForKey:@"isSendingResults"];
    }
}

#pragma mark - WUTPlotViewDataSource

- (long long)numberOfSamplesForPlotView:(WUTPlotView *)plotView{
    return (long long)[resultsArray_ count];
}

- (WUTResult *)plotView:(WUTPlotView *)plotView sampleAtIndex:(long long)index{
    return [resultsArray_ objectAtIndex:(NSInteger)index];
}

#pragma mark - SocketRocketDelegate

- (void)webSocket:(SRWebSocket *)webSocket didFailWithError:(NSError *)error{
    DDLogError(@"did fail with error %@", [error localizedDescription]);
    [webSocket close];

}

- (void)webSocket:(SRWebSocket *)webSocket didCloseWithCode:(NSInteger)code reason:(NSString *)reason wasClean:(BOOL)wasClean{
    DDLogError(@"did close with code %d, reason:%@, was clean:%d", code, reason, wasClean);
    

}

- (void)webSocket:(SRWebSocket *)webSocket didReceiveMessage:(id)message{
    
    
    NSData *data = [message dataUsingEncoding:NSUTF8StringEncoding];
    
    if (!data) {
//        [self errorWithMessage:@"Can't decode data from WebSocket"];
        [webSocket close];
        return;
    }
    
    NSError * __autoreleasing error = nil;
    
    NSDictionary *dictonaryStatus = [NSJSONSerialization JSONObjectWithData:data
                                                                    options:NSJSONWritingPrettyPrinted
                                                                      error:&error];
    DDLogInfo(@"data:%@", dictonaryStatus);
    
    NSArray *arraySamples = dictonaryStatus[@"measurements"];
 
    Measurement *sample = [Measurement measurementWithDict:arraySamples[0]];
    
    [self sendMeasurement:sample];
}

@end
