//
//  TKMainViewController.h
//  ELHEP
//
//  Created by Tomasz Kuźma on 2/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "WUTViewController.h"

@class WUTDiscovery;

@interface TKMainViewController : WUTViewController <
                                                    NSURLConnectionDelegate,
                                                    UITableViewDelegate,
                                                    UITableViewDataSource,
                                                    NSFetchedResultsControllerDelegate
                                                    >


@property (strong, nonatomic) UITableView *tableView;

@property (strong, nonatomic) NSMutableArray *devicesArray;

@property (strong, nonatomic) UIPopoverController *flipsidePopoverController;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@property (strong, nonatomic) WUTDiscovery *discovery;

@end


@interface UIAlertView (TKExtensions)

+ (void)showAlertWithTitle:(NSString *)title message:(NSString *)message;

@end