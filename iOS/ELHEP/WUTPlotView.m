//
//  WUTPlotView.m
//  ELHEP
//
//  Created by Tomasz Kuźma on 5/30/12.
//  Copyright (c) 2012 Tomasz Kuzma (mapedd@mapedd.com) Warsaw University of Technology. All rights reserved.
//

#import "WUTPlotView.h"
#import "NSDate-TKExtensions.h"
#import "WUTResult.h"

#import "DDLog.h"
#import "DDTTYLogger.h"
// Log levels: off, error, warn, info, verbose
static const int ddLogLevel = LOG_LEVEL_VERBOSE;

#define DATE_FORMAT @"HH:mm:ss:SSS"
#define DATE_FORMAT_H @"h"
#define DATE_FORMAT_M @"m"
#define DATE_FORMA_S  @"s"
#define DATE_FORMAT_MS @"A"


@interface WUTPlotView (){
    struct {
        unsigned int dataSourceNumberOfSamples:1;
        unsigned int dataSourceSampleForIndex:1;
    } _dataSourceFlags;
}


@end

@implementation WUTPlotView

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(!self)  return nil;
    
    self.dateFormatter = [[NSDateFormatter alloc] init];
    [self.dateFormatter setDateFormat:DATE_FORMAT];
//    [self.dateFormatter setDateStyle:NSDateFormatterShortStyle];
    
    self.backgroundColor = [UIColor whiteColor];
    self.timeColor = [UIColor whiteColor];
    self.unitColor = [UIColor whiteColor];
    self.boundsColor = [UIColor blackColor];
    self.textColor = [UIColor blueColor];
    self.lineColor = [UIColor redColor];
    
    self.timeFont = [UIFont boldSystemFontOfSize:12.0f];
    self.unitFont = [UIFont boldSystemFontOfSize:12.0f];
    
    self.minValue = 0.0f;
    self.maxValue = 1.0f;
    
    self.unitsMarginWidth = 30.0f;
    self.timeMarginWidth = 30.0f;
    
    self.timeLabelOffset = 120;
    self.unitLabelOffset = 0.1f;
    
    self.opaque = YES;
    
    return self;
}

- (id)initWithFrame:(CGRect)frame dataSource:(id<WUTPlotViewDataSource>)dataSource{
    self = [self initWithFrame:frame];
    if (!self) return nil;
    
    self.dataSource = dataSource;

    return self;
}

- (void)setDataSource:(id<WUTPlotViewDataSource>)dataSource{
    
    if (_dataSource != dataSource) {
        _dataSourceFlags.dataSourceSampleForIndex = [dataSource respondsToSelector:@selector(plotView:sampleAtIndex:)];
        _dataSourceFlags.dataSourceNumberOfSamples = [dataSource respondsToSelector:@selector(numberOfSamplesForPlotView:)];
        
        _dataSource = dataSource;
    }   
}

- (void)didMoveToSuperview{
    [self setNeedsDisplay];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect{
    
    
    NSDate *todayStart = [[NSDate date] TKDateByMovingToBeginningOfDay];
    
    static int k = 0;
    
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    CGRect plotRect = CGRectMake(_unitsMarginWidth, 0.0f, CGRectGetWidth(rect)-_unitsMarginWidth, CGRectGetHeight(rect) - _timeMarginWidth);
    CGRect unitRect = CGRectMake(0.0f, 0.0f, _unitsMarginWidth, CGRectGetHeight(rect) - _timeMarginWidth);
    CGRect timeRect = CGRectMake(_unitsMarginWidth, CGRectGetHeight(rect) - _timeMarginWidth, CGRectGetWidth(rect)-_unitsMarginWidth,  _timeMarginWidth);
    CGRect cornerRect = CGRectMake(0.0, CGRectGetHeight(rect) - _timeMarginWidth, _unitsMarginWidth,  _timeMarginWidth);
    

    // Draw background
    CGContextSetFillColorWithColor(ctx, self.backgroundColor.CGColor);
    CGContextFillRect(ctx, cornerRect);
    
    // Draw background      
    CGContextSetFillColorWithColor(ctx, self.backgroundColor.CGColor);
    CGContextFillRect(ctx, plotRect);
    
    // Draw background
    CGContextSetFillColorWithColor(ctx, self.unitColor.CGColor);
    CGContextFillRect(ctx, unitRect);
    
    // Draw background
    CGContextSetFillColorWithColor(ctx, self.timeColor.CGColor);
    CGContextFillRect(ctx, timeRect);
    
    // Draw bounds
    CGContextSetStrokeColorWithColor(ctx, self.boundsColor.CGColor);
    CGContextSetLineWidth(ctx, 1.0f);
    CGContextStrokeRect(ctx, rect);
    CGContextStrokeRect(ctx, plotRect);
    
    {// Draw units
        CGContextSaveGState(ctx);
        
        CGFloat span = _maxValue - _minValue;
        NSInteger steps = span / _unitLabelOffset;
        
        for (int i = 0; i < steps; i ++) {
            
            CGFloat unit = _maxValue - (span / steps) * i;
            
            CGContextSetFillColorWithColor(ctx, [UIColor redColor].CGColor);
            CGContextSetStrokeColorWithColor(ctx, [UIColor blueColor].CGColor);
            NSString *timeString = [NSString stringWithFormat:@"%1.2f",unit];
            
            CGPoint timePoint = CGPointMake(0, 0);
            CGSize size = [timeString sizeWithFont:_timeFont];
            CGContextStrokeRect(ctx, CGRectMake(timePoint.x, timePoint.y, size.width, size.height));
            
            [timeString drawAtPoint:timePoint withFont:_timeFont];
            
            CGContextTranslateCTM(ctx, 0.0f, plotRect.size.height / steps);
        }
        
        CGContextRestoreGState(ctx);
    }
    
    // Invert CTM to use 'normal' coordinates
    CGContextSaveGState(ctx);
    CGContextTranslateCTM(ctx, _unitsMarginWidth, CGRectGetMaxY(rect));
    CGContextScaleCTM(ctx, 1.0f, -1.0f);
    
    NSInteger count;
    
    // If delegate responds to selector, get how many there are ready to draw
    if (_dataSourceFlags.dataSourceNumberOfSamples){
        count = [self.dataSource numberOfSamplesForPlotView:self];
    }
    else {
        CGContextRestoreGState(ctx);
        return;
    }
    
    
    NSInteger width = (NSInteger)CGRectGetWidth(rect) - _unitsMarginWidth;
    CGFloat height = CGRectGetHeight(rect) - _timeMarginWidth;
    
    {
        
        CGContextSetStrokeColorWithColor(ctx, self.lineColor.CGColor);

        
        CGMutablePathRef path = CGPathCreateMutable();
        CGPathMoveToPoint(path, NULL, 0, 0);
        
        
        
        for (int i = 0; i < MIN(width, count); i++) {
            WUTResult *result = [self.dataSource plotView:self sampleAtIndex:count - 1 - i];
            CGPathAddLineToPoint(path, NULL, i, _timeMarginWidth + result.value * height);
            
            if (i % _timeLabelOffset== 0) {
                CGContextSaveGState(ctx);
                CGContextScaleCTM(ctx, 1.0f,-1.0f);
                CGContextTranslateCTM(ctx, 0.0f, -_timeMarginWidth);
                {
                    CGContextSetFillColorWithColor(ctx, _textColor.CGColor);
                    
                    NSString *timeString = [_dateFormatter stringFromDate:result.timeStamp];
                    
                    CGPoint timePoint = CGPointMake(k, 0);
                    [timeString drawAtPoint:timePoint withFont:_timeFont];
                }
                CGContextRestoreGState(ctx);
            }
            
            k++;
            
            if (k == MIN(width, count)) {
                k = 0;
            }
        }
        
        // Add path to context, set width and stoke it
        CGContextAddPath(ctx, path);
        CGContextSetLineWidth(ctx, 1.0f);
        CGContextStrokePath(ctx);
        CGPathRelease(path);
    }
    

    // Restore default CTM at the end
    CGContextRestoreGState(ctx);

    
}



@end
