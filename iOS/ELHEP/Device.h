//
//  Device.h
//  ELHEP
//
//  Created by Tomasz Kuźma on 11/21/12.
//  Copyright (c) 2012 Tomasz Ku≈∫ma. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Probe;

@interface Device : NSManagedObject

@property (nonatomic, retain) NSString * address;
@property (nonatomic, retain) NSDate * dateAdded;
@property (nonatomic, retain) NSNumber * deviceID;
@property (nonatomic, retain) NSString * deviceName;
@property (nonatomic, retain) NSNumber * interface;
@property (nonatomic, retain) NSNumber * verID;
@property (nonatomic, retain) NSNumber * isConnected;
@property (nonatomic, retain) NSSet *uniqueProbes;
@end

@interface Device (CoreDataGeneratedAccessors)

- (void)addUniqueProbesObject:(Probe *)value;
- (void)removeUniqueProbesObject:(Probe *)value;
- (void)addUniqueProbes:(NSSet *)values;
- (void)removeUniqueProbes:(NSSet *)values;

@end
