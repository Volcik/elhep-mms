package com.example.elhep_mms;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import de.tavendo.autobahn.WebSocket;
import de.tavendo.autobahn.WebSocketConnection;
import de.tavendo.autobahn.WebSocketConnectionHandler;
import de.tavendo.autobahn.WebSocketException;
import de.tavendo.autobahn.WebSocketOptions;

// Manages WebSockets connection with measurement device
public class Websocket_connection extends Thread {	
	
	public final static int ARG_WS_NEW_DATA = 1;	
	public final static int ARG_WS_CLOSED_CONN = 2;
	
	private Handler handler_;
	private Handler ws_handler;
	private Device_data device_data_;
	private WebSocket ws_conn;
	private boolean close_connection = false;
	
	// handler for communication with main thread
	public Websocket_connection(String IP) {
		handler_ = MainActivity.h_;
		device_data_ = new Device_data();
		device_data_.IP = IP;
		device_data_.type = Device_data.CONNECTION_WIFI;		
	}	
	
	public void closeConn() {
		
		close_connection = true;
		
		if (ws_conn.isConnected())
			ws_conn.disconnect();
		
		// WebsocketConnector class not closing...
		// those lines should be implemented...
		//ws_conn = null;
		//ws_handler.getLooper().quit();
		//	Looper.myLooper().quit();
		
	}
	
	public void sendData(Device_data data) {
		
		// send data
		ws_conn.sendTextMessage(data.JSON_data.toString());
		//ws_conn.sendTextMessage(data.JSON_data.toString().getBytes());
	}
	
	
	public void run() {		
		
		Looper.prepare();
		
		ws_handler = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				
			}
		};				
		
		WebSocketOptions ws_options = new WebSocketOptions();
		ws_conn = new WebSocketConnection();
		
		try {
			ws_conn.connect("ws://" + device_data_.IP + ":" + MainActivity.WEBSOCKET_PORT + "/",
					new String[] {"elhep-test"}, new WebSocketConnectionHandler() { // subprotocol of MMS device system
						@Override
						public void onOpen() {
							// connection is opened
							// next thing it to get JSON data from device
							//System.out.println("WebSockets connection with " + device_data_.IP + " opened...");
							Log.d("WebSocket", "WebSockets connection with " + device_data_.IP + " opened...");
							//device_data_.handler = this;
						}
						
						@Override
						public void onTextMessage(String payload) {
							// Get text
							//System.out.println("WebSockets " + device_data_.IP + " new message...");
							Log.d("WebSocket", "WebSockets " + device_data_.IP + " new message...");

							// create JSON object
							try {
								device_data_.JSON_data = new JSONObject(payload);
							} catch (JSONException e) {
								System.out.println("JSON object creation error.");
								Log.d("WebSocket", "WebSockets " + device_data_.IP + " JSON object creation error.");
							}
							// send it to main thread
							Message msg = handler_.obtainMessage();
							msg.arg1 = ARG_WS_NEW_DATA;
							msg.obj = device_data_;
							handler_.sendMessage(msg);
							
						}
						
						@Override
						public void onClose(int code, String reason) {
							Log.d("WebSocket", "Websockets connection with " + device_data_.IP + " closed...");
							
							if (close_connection) // connection closed by user (don't send data to main thread)
								return;
							
							// Connection with server closed (by remote host)
							// sending info to main thread
							Message msg = handler_.obtainMessage();
							msg.arg1 = ARG_WS_CLOSED_CONN;
							msg.obj = device_data_;
							handler_.sendMessage(msg);
							
							//System.out.println("Websockets connection with " + device_data_.IP + " closed...");
						}					
					}, ws_options);
			
		} catch (WebSocketException e) {
			// TODO Auto-generated catch block
			Log.d("WebSocket", "WebSockets " + device_data_.IP + " exception...");
			//System.out.println("WebSockets " + device_data_.IP + " exception...");
		}
				
		Looper.loop();
		
	}
	
}
