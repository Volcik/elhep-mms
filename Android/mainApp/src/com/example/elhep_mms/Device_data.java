package com.example.elhep_mms;

import org.json.JSONObject;


// Data pack for communication with Measurement device
public class Device_data {
	
	public final static int CONNECTION_WIFI = 1;
	public final static int CONNECTION_BLUETOOTH = 2;
	
	public String IP; // IP of Measurement device
	public int type; // type of connection (Wifi/ Bluetooth)
	public Object handler; // connection handler
	//public Thread Connection_handler; // Handler to WebSocket thread or Bluetooth for communication
	public JSONObject JSON_data; // JSON data (data for communication with Measurement device)

}
